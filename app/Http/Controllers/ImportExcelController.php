<?php

namespace App\Http\Controllers;

use App\Http\Requests\UploadExcelRequest;
use App\Imports\MultiSheetImport;
use App\User;


class ImportExcelController extends Controller
{
    public function index()
    {
        $users = User::with('roles')->orderBy('id','DESC')->get();
        return view('import.users',compact('users'));
    }


    public function import(UploadExcelRequest $request)
    {
        $path = $request->file('file')->getRealPath();
        $import = new MultiSheetImport();
        $import->import($path);


        return  back()->withStatus('Import Success File Excel');
    }

}
