<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class MultiSheetImport implements WithMultipleSheets
{
    use Importable;


    public function sheets(): array
    {

        return [
            'roles' => new RolesImport(),
            'users' => new UsersImport(),
        ];
    }



}
