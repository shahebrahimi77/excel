<?php

namespace App\Imports;

use App\Role;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToArray;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class RolesImport extends Import implements ToCollection,WithHeadingRow,ToArray
{

    /**
     * @param Collection $collections
     */

    public function collection(Collection $collections)
    {
        foreach ($collections as $collection)
        {
            if ($role = Role::whereName($collection['name'])->first()){
               $role->update([
                   'name'=>$collection["name"]
               ]);
            }else{
                Role::create([
                    'name'=> $collection['name']
                ]);
            }
        }
    }
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => ['required']
        ];
    }
    /**
     * @param array $array
     */
    public function array(array $array)
    {
        static::$data = $array;
    }
}
