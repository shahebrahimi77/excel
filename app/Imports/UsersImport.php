<?php

namespace App\Imports;

use App\Role;
use App\User;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;


class UsersImport extends  Import implements ToCollection,WithHeadingRow,WithValidation
{
    /**
     * @param Collection $collections
     */
    public function collection(Collection $collections)
    {
        foreach ($collections as $collection) {
             $user =User::create([
                'name' => $collection['name'],
                'gender' => $collection['gender'],
                'city' => $collection['city'],
                'country' => $collection['country'],
                'address' => $collection['address'],
            ]);
            $roleId = explode(',', $collection['role']);
            $data = static::$data;
            foreach ($data as $role){
                if (in_array($role['id'],$roleId)){
                    $name = $role['name'];
                    $role =Role::whereName($name)->first();
                    $user->roles()->attach($role->id) ;
                }

            }
        }

    }
    /**
     * @return array
     */
    public function rules(): array
    {
        return  [
            'name' => ['required'],
            'gender' => ['required'],
            'city' => ['required'],
            'country' => ['required'],
            'address' => ['required'],
        ];
    }



}

