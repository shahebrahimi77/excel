<form action="{{ route($routeName) }}" method="POST" enctype="multipart/form-data">
    @csrf

    <div class="form-group">
        <table class="table">
            <tr>
                <td width="30%" align="left">
                    <label for="">Please Select a File Excel</label>
                </td>
                <td width="40%" align="center">
                    <input type="file" name="file">
                </td>
                <td width="30%" align="right">
                    <input class="btn btn-primary" type="submit" name="upload" value="Import">
                </td>

            </tr>
        </table>
    </div>
</form>
