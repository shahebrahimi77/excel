@extends('import.master')
@section('content')
    <div class="container">
    <h3 align="center">Import  File User  Excel </h3>
    @include('errors')
    @include('import.form-imports',['routeName' => 'users.import'])
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">name</th>
            <th scope="col">city</th>
            <th scope="col">country</th>
            <th scope="col">gender</th>
            <th scope="col">address</th>
            <th scope="col">roles</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <th scope="row">{{$user->id}}</th>
                <td>{{$user->name}}</td>
                <td>{{$user->city}}</td>
                <td>{{$user->country}}</td>
                <td>{{$user->gender}}</td>
                <td>{{$user->address}}</td>
                <td>
                    @foreach ($user->roles as $role)
                        | {{$role->name}} |
                    @endforeach
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@endsection
