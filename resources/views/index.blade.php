@extends('import.master')
@section('content')
    <div class="container">
        <h3 align="center">Import File </h3>

        <div align="center">
            <a href="{{ route('users.index') }}" class="btn btn-primary">Import Excel</a>
        </div>
    </div>
@endsection
