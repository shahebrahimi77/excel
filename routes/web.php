<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function (){
    return view('index');
});
//Route User Import
Route::prefix('users')->name('users.')->group(function ($router){
    $router->get('import', 'ImportExcelController@index')->name('index');
    $router->post('import', 'ImportExcelController@import')->name('import');
});




